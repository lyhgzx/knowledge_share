知识分享网
===============

技术选型
===============
1、后端
* 核心框架：Spring boot 
* 数据访问：mybatis
* 通用mapper：tk.mybatis
* 分页： pagehelper
* 数据库引擎： mysql
* 数据库连接池： druid
* 视图引擎：thymeleaf

1、前端
* JS框架：jQuery
* 树结构控件：jQuery zTree
* UI框架：HUI


部署
===============
1.mvn -package命令
2.java -jar knowledge_share.jar