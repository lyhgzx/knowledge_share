/*
MySQL Backup
Source Server Version: 5.5.53
Source Database: knowledge_share
Date: 2017/8/14 17:40:03
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
--  Table structure for `item_content`
-- ----------------------------
DROP TABLE IF EXISTS `item_content`;
CREATE TABLE `item_content` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `type_id` bigint(20) DEFAULT NULL,
  `rm` varchar(255) DEFAULT NULL,
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Table structure for `item_type`
-- ----------------------------
DROP TABLE IF EXISTS `item_type`;
CREATE TABLE `item_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `parent_id` bigint(20) DEFAULT NULL COMMENT '父节点id',
  `parent_name` varchar(168) CHARACTER SET utf8 DEFAULT NULL COMMENT '父节点名称',
  `sort` bigint(20) DEFAULT NULL COMMENT '排序号',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `modify_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
--  Records 
-- ----------------------------
INSERT INTO `item_content` VALUES ('1','《成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:27:20',NULL), ('2','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:49:22',NULL), ('3','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:49:41',NULL), ('4','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:50:03',NULL), ('5','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:50:19',NULL), ('6','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:50:32',NULL), ('7','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:50:59',NULL), ('8','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:51:02',NULL), ('9','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:51:06',NULL), ('10','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:51:11',NULL), ('11','成神之路-基础篇》JVM——Java内存相关(已完结)','http://www.hollischuang.com/archives/1003','8',NULL,'2017-08-08 14:51:13',NULL), ('12','下载安装配置与使用MySQL-5.7.12-winx64.zip','http://jingyan.baidu.com/article/0f5fb0991fef3c6d8334ea23.html','6',NULL,'2017-08-08 16:45:01',NULL), ('15','fadsf','http://www.layui.com/laypage/','8','','2017-08-08 17:35:35','2017-08-08 17:35:35'), ('16','HTML 视频','http://www.w3school.com.cn/html/html_video.asp','2','','2017-08-08 17:36:33','2017-08-08 17:36:33');
INSERT INTO `item_type` VALUES ('1','前端','0',NULL,'0',NULL,NULL), ('2','1、html','1','java学习','0',NULL,NULL), ('3','2、css','1','java学习',NULL,NULL,NULL), ('4','数据库知识','0',NULL,'2',NULL,NULL), ('5','1、sqlserver','4','数据库知识','0',NULL,NULL), ('6','2、mysql','4','数据库知识','1',NULL,NULL), ('7','java','0',NULL,'1',NULL,NULL), ('8','jvm','7','java','0',NULL,NULL);
