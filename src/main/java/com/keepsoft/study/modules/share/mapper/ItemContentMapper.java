package com.keepsoft.study.modules.share.mapper;

import com.keepsoft.study.code.base.MyMapper;
import com.keepsoft.study.modules.share.model.ItemContent;

public interface ItemContentMapper extends MyMapper<ItemContent> {
}