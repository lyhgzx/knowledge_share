package com.keepsoft.study.modules.share.mapper;

import com.keepsoft.study.code.base.MyMapper;
import com.keepsoft.study.modules.share.model.ItemType;

public interface ItemTypeMapper extends MyMapper<ItemType> {
}