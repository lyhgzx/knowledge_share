package com.keepsoft.study.modules.share.model;

import com.keepsoft.study.code.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;
@Data
@Table(name = "item_content")
public class ItemContent extends BaseEntity {


    private String title;

    private String url;

    @Column(name = "type_id")
    private Long typeId;

    private String rm;


}