package com.keepsoft.study.modules.share.model;

import com.keepsoft.study.code.base.BaseEntity;
import lombok.Data;

import java.util.Date;
import javax.persistence.*;
@Data
@Table(name = "item_type")
public class ItemType extends BaseEntity {


    private String name;

    /**
     * 父节点id
     */
    @Column(name = "parent_id")
    private Long parentId;

    /**
     * 父节点名称
     */
    @Column(name = "parent_name")
    private String parentName;

    /**
     * 排序号
     */
    private Long sort;

}