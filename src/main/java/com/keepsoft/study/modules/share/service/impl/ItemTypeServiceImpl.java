package com.keepsoft.study.modules.share.service.impl;

import com.github.pagehelper.PageInfo;
import com.keepsoft.study.code.base.BaseServiceImpl;
import com.keepsoft.study.modules.share.model.ItemType;
import com.keepsoft.study.modules.share.service.ItemTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * @author liuyang
 * @desc 栏目类型服务实现类
 **/
@Service
@Transactional
public class ItemTypeServiceImpl  extends BaseServiceImpl<ItemType> implements ItemTypeService {

    @Transactional(readOnly=true)
    @Override
    public PageInfo<HashMap<String, Object>> findPage(Integer pageNum, Integer pageSize, String typeId) throws Exception {
        HashMap<String,Object> map=new HashMap<>();
        map.put("typeId",typeId);
       return  this.findPageListCommon(pageNum,pageSize,map);
    }
}
