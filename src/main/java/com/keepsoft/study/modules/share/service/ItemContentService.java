package com.keepsoft.study.modules.share.service;

import com.github.pagehelper.PageInfo;
import com.keepsoft.study.code.base.BaseService;
import com.keepsoft.study.modules.share.model.ItemContent;
import com.keepsoft.study.modules.share.model.ItemType;

import java.util.HashMap;

/**
 * @author liuyang
 * @create 2017-08-08 12:53
 * @desc 栏目内容服务接口
 **/
public interface ItemContentService extends BaseService<ItemContent> {



}
