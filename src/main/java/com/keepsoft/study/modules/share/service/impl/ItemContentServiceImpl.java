package com.keepsoft.study.modules.share.service.impl;

import com.github.pagehelper.PageInfo;
import com.keepsoft.study.code.base.BaseServiceImpl;
import com.keepsoft.study.modules.share.model.ItemContent;
import com.keepsoft.study.modules.share.model.ItemType;
import com.keepsoft.study.modules.share.service.ItemContentService;
import com.keepsoft.study.modules.share.service.ItemTypeService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;

/**
 * @author liuyang
 * @desc 栏目内容服务实现类
 **/
@Service
@Transactional
public class ItemContentServiceImpl extends BaseServiceImpl<ItemContent> implements ItemContentService {


}
