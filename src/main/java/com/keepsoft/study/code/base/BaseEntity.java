package com.keepsoft.study.code.base;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

/**
 * @author liuyang
 * @create 2017-08-08 12:47
 * @desc 基础实体
 **/
@Data
public abstract class BaseEntity implements java.io.Serializable{
    @Id
    @Column(name = "Id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private Date createTime;

    private Date modifyTime;
}
