package com.keepsoft.study.code.base;

import tk.mybatis.mapper.common.IdsMapper;
import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.MySqlMapper;

import java.util.HashMap;
import java.util.List;

/**
 * @author liuyang
 * @create 2017-08-08 12:42
 * @desc 通用mapper
 **/
public interface MyMapper<T> extends Mapper<T>, MySqlMapper<T>, IdsMapper<T> {
    //TODO
    //FIXME 特别注意，该接口不能被扫描到，否则会出错

    List<HashMap<String, Object>> findPageListCommon(HashMap<String, Object> map);

}