/**
 * Created by Administrator on 2017/8/8.
 */

$(function () {

    //提交
    $("#add").click(function () {

        $("#form-admin-add").ajaxSubmit({
            type: 'post',
            url: contextPath + "share",
            dataType:"json",
            success: function(data){
                if(data.status == "success"){
                    succeedMessage(data.message);
                    var index = parent.layer.getFrameIndex(window.name);
                    parent.location.reload();
                    parent.layer.close(index);
                }else {
                    errorMessage(data.message);
                }
            }
        });


    })
})